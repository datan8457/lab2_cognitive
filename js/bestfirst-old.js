var searchNode = {
  construct : 
    function(obj, state, quality, parent, action) {
      obj.state = state;
      obj.quality = quality;
      obj.parent = parent;
      obj.action = action;
      obj.prev = null;
      obj.next = null;
    },
  isGoal : function() {return false;},
  getChildren : function() {return [];},
  hasAncestor : 
    function(state) {
      var curr = this.parent;
      while (curr) {
        if (state.toString()==curr.state.toString())
          return true;
        curr = curr.parent;
      }
    }
};

// Note: an industrial strength solution should
//   use a priority queue rather than a normal list.
function Agenda() {
  this.list = null;
  this.add = function(node) {
    if (!this.list) {
      this.list = node;
      this.list.prev = null;
      this.list.next = null;
      return;
    }
    var curr = this.list;
    var prev = null;
    while (curr) {
      if (node.quality>curr.quality) {
        if (curr.prev)
          curr.prev.next = node;
        else
          this.list = node;
        curr.prev = node;
        node.next = curr;
        node.prev = prev;
        return;
      }
      if (!curr.next)
        break;
      prev = curr;
      curr = curr.next;
    }
    curr.next = node;
    node.next = null;
    node.prev = curr;
  };
  this.pop = function() {
    if (!this.list)
      return null;
    var value = this.list;
    this.list = value.next;
    value.next = null;
    value.prev = null;
    if (this.list)
      this.list.prev = null;
    return value;
  }
  this.remove = function(node) {
    if (this.list===node) {
      this.list = node.next;
      if (node.next)
        node.next.prev = null;
      return;
    }
    node.prev.next = node.next;
    if (node.next)
      node.next.prev = node.prev;
  }
  this.dump = function() {
    console.log("Agenda Dump");
    var curr = this.list;
    while (curr) {
      console.log(curr.state,curr.quality);
      curr = curr.next;
    }
    console.log("End Dump");
  }
}

function bestFirstSearch(initialNode, useClosedList, noLooping, debug) {
  // loop prevention not needed with closed list
  if (useClosedList)
    noLooping = false; 
  var nClosed = 0;
  var agenda = new Agenda();
  var open = {};
  var closed = {};
  agenda.add(initialNode);
  var key = initialNode.state.toString();
  open[key] = initialNode;
  var cnt = 0;
  while (agenda) {
    var sn = agenda.pop();
    cnt ++;
    if (debug)
      console.log(cnt,"processing node with state",sn.state.toString(),"quality",sn.quality);
    if (sn.isGoal())
      return sn;
    if (useClosedList) {
      var key = sn.state.toString();
      closed[key] = true;
      ++ nClosed;
    }
    var childL = sn.getChildren();
    for ( var i=0; i<childL.length; ++i ) {
      var child = childL[i];
      if (useClosedList) {
        var key = child.state.toString();
        if (closed[key])  
          continue;
        if (open[key]) {
          if (child.quality < open[key].quality)
            continue;
          else
            agenda.remove(open[key]);
        }
        open[key] = child;
      }
      else if (noLooping) {
        if ( child.hasAncestor(child.state) ) 
          continue;
      }
      agenda.add( child );
    }
  }
}

